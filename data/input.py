#! /usr/bin/python
# -*- coding:utf-8 -*-

# usage
# input image must have 111pix x 111pix size

# halftone image -> 37x37 numpy.array 0~511 represent sub-module
# imp map -> 37x37 numpy.array [0,1] represent average of imp map
# original qr code -> 37x37 numpy.array 0 or 511 represent sub-module

import sys
import numpy
import qrcode
from PIL import Image

const_dict = {}
const_dict[5] = [(30,30)]

def qr_constant_simbol_mask(ver,im_array,mask_image):
    size = 17 + 4*ver
    # upper left
    for i in range(9):
        for j in range(9):
            im_array[i,j] = -1
    # upper right
    for i in range(size-8,size,1):
        for j in range(9):
            im_array[i,j] = -1
    # lowler left
    for i in range(9):
        for j in range(size-8,size,1):
            im_array[i,j] = -1
    # timing pattern horizontal
    for i in range(size):
        im_array[i,6] = -1
    # timing pattern vartical
    for j in range(size):
        im_array[6,j] = -1
    for k in const_dict[ver]:
        for i in range(k[0]-2,k[0]+3,1):
            for j in range(k[1]-2,k[1]+3,1):
                im_array[i,j] = -1

    for i in range(size):
        for j in range(size):
            sum = 0
            for k in range(3):
                for l in range(3):
                    sum += mask_image[i*3+k,j*3+l]
            if(sum == 0):
                im_array[i,j] = -1

    return im_array

# main
if __name__ == '__main__':

    # create original qr code
    qr = qrcode.QRCode(
        version=int(sys.argv[3]),
        error_correction=qrcode.constants.ERROR_CORRECT_H,
        box_size=1,
        border=0,
        )
    qr.add_data(sys.argv[4])
    qr.make(fit=True)
    qrver = max(int(sys.argv[3]),qr.version)
    size = 17+4 * qrver
    print size
    qr_img = qr.make_image()
    qr_ar = qr_img.load()
    # output original qr code
    qr_img.convert('1').save('./outdata/original_qr.bmp')
    qr_array = numpy.array([[0 for i in range(size)] for j in range(size)])
    for i in range(size):
        for j in range(size):
            qr_array[i,j] = qr_ar[i,j] * 511/255

    # open input file
    halftone = Image.open(sys.argv[1]).resize((size*3,size*3)).convert('1')
    mask_im = Image.open(sys.argv[2]).resize((size*3,size*3)).convert('1')
    halftone.save('./outdata/halftone.bmp')
    halftone_ar = halftone.load()
    mask_ar = mask_im.load()
    halftone_array = numpy.array([[0 for i in range(size)] for j in range(size)])
    for i in range(size):
        for j in range(size):
            halftone_array[i,j] = halftone_ar[i*3,j*3]%2*256+halftone_ar[i*3+1,j*3]%2*128+halftone_ar[i*3+2,j*3]%2*64+halftone_ar[i*3,j*3+1]%2*32+halftone_ar[i*3+1,j*3+1]%2*16+halftone_ar[i*3+2,j*3+1]%2*8+halftone_ar[i*3,j*3+2]%2*4+halftone_ar[i*3+1,j*3+2]%2*2+halftone_ar[i*3+2,j*3+2]%2


    # under here processing is start
    # set constant mask
    hf_array = qr_constant_simbol_mask(qrver,halftone_array,mask_ar)

    f = open('./temp_out/hf_mat.txt','w')
    g = open('./temp_out/qr_mat.txt','w')
    g.write(str(qrver))
    g.write('\n')
    for i in range(size):
        for j in range(size):
            f.write(str(hf_array[j,i]))
            f.write('\n')
            g.write(str(qr_array[j,i]))
            g.write('\n')
    f.close()
    g.close()
