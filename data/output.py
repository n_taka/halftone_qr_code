#! /usr/bin/python
# -*- coding:utf-8 -*-

# usage
# python enshu3.py <input image> <embed string>
# input image must have 111pix x 111pix size

# halftone image -> 37x37 numpy.array 0~511 represent sub-module
# imp map -> 37x37 numpy.array [0,1] represent average of imp map
# original qr code -> 37x37 numpy.array 0 or 511 represent sub-module

import sys
import numpy
import qrcode
from PIL import Image

def qr_constant_simbol_unmask(im_array,qr_array,size):
    for i in range(size):
        for j in range(size):
            if(im_array[i,j] == -1):
                im_array[i,j] = qr_array[i,j]
    return im_array

def qr_output(qr_array,filename,size):
    outimage = Image.new("1",(size*3+24,size*3+24),255)
    pix = outimage.load()
    for i in range(size):
        for j in range(size):
            pix[(i+4)*3,(j+4)*3] = qr_array[i,j]/256
            pix[(i+4)*3+1,(j+4)*3] = (qr_array[i,j]/128)%2
            pix[(i+4)*3+2,(j+4)*3] = (qr_array[i,j]/64)%2
            pix[(i+4)*3,(j+4)*3+1] = (qr_array[i,j]/32)%2
            pix[(i+4)*3+1,(j+4)*3+1] = (qr_array[i,j]/16)%2
            pix[(i+4)*3+2,(j+4)*3+1] = (qr_array[i,j]/8)%2
            pix[(i+4)*3,(j+4)*3+2] = (qr_array[i,j]/4)%2
            pix[(i+4)*3+1,(j+4)*3+2] = (qr_array[i,j]/2)%2
            pix[(i+4)*3+2,(j+4)*3+2] = qr_array[i,j]%2
    outimage.save(filename)
    return


# main
if __name__ == '__main__':
    # open and read qr output matrix
    f = open('./temp_out/qr_mat.txt','r')
    size = int(f.readline()) * 4 + 17
    qr_array = numpy.array([[0 for i in range(size)] for j in range(size)])
    k = 0
    for i in f:
        qr_array[k%size,k/size] = int(i.rstrip())
        k += 1
    f.close()

    g = open('./temp_out/output.txt','r')
    outdata = numpy.array([[0 for i in range(size)] for j in range(size)])
    k = 0
    for i in g:
        outdata[k%size,k/size] = int(i.rstrip())
        k += 1
    g.close()

    outdata1 = qr_constant_simbol_unmask(outdata,qr_array,size)
    qr_output(outdata1,'./outdata/'+sys.argv[1],size)
