#! /usr/bin/python
# -*- coding:utf-8 -*-

import math

def ssim(a,b):
    if a == b:
        return 1.0
    elif a < 0 or a > 511 or b < 0 or b > 511:
        return -2.0
    else:
        c1 = (0.01*1)**2
        c2 = (0.03*1)**2
        pix1 = [0 for i in range(9)]
        pix2 = [0 for i in range(9)]
        sum1 = 0.0
        sum2 = 0.0
        for i in range(9):
            pix1[i] = (a/(2**i))%2
            sum1 += (a/(2**i))%2
            pix2[i] = (b/(2**i))%2
            sum2 += (b/(2**i))%2
        ave1 = sum1/9.0
        ave2 = sum2/9.0
        sum3 = 0.0
        sum4 = 0.0
        sum5 = 0.0
        for i in range(9):
            sum3 += (pix1[i] - ave1)**2
            sum4 += (pix2[i] - ave2)**2
            sum5 += (pix1[i] - ave1)*(pix2[i] - ave2)
        var1 = math.sqrt(sum3/9.0)
        var2 = math.sqrt(sum4/9.0)
        covar = sum5/9.0
        ssim = (2.0*ave1*ave2 + c1)*(2.0*covar + c2) / ((ave1**2 + ave2**2 + c1)*(var1**2 + var2**2 + c2))
        return ssim

f = open('matdata2.txt','w')
for i in range(512):
    for j in range(512):
        ans = (1.0-ssim(i,j))/2.0
        if ans < 0:
            print "out!!"
        f.write(str(ans))
        f.write('\n')
    print str(i)+"loop"
f.close()
