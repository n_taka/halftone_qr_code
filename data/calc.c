#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double energy_label_set(int i,int j,int a,int b,int* hf_array,int* data_array,int* qr_array,double* ssim,int size){
  double w_s = 0.2;
  double ene_a;
  double ene_b;
  int k,l;

  if(data_array[i*size+j] == a){
    ene_a = 0.0;
    for(k=-1;k<2;k+=2){
      for(l=-1;l<2;l+=2){
	if(i+k >= 0 && i+k <= size-1 && j+l >= 0 && j+l <= size-1){
	  if(hf_array[(i+k)*size+j+l] != -1){
	    ene_a += ssim[a*512 + data_array[(i+k)*size + j+l]] * exp(-ssim[hf_array[i*size+j]*512+hf_array[(i+k)*size+j+l]]);
	  }
	}
      }
    }
    ene_a = w_s * ene_a + ssim[a*512+hf_array[i*size+j]];
    if(a/16%2 != qr_array[i*size+j]%2){
      ene_a += 100;
    }

    ene_b = 0.0;
    for(k=-1;k<2;k+=2){
      for(l=-1;l<2;l+=2){
	if(i+k >= 0 && i+k <= size-1 && j+l >= 0 && j+l <= size-1){
	  if(hf_array[(i+k)*size+j+l] != -1){
	    ene_b += ssim[b*512 + data_array[(i+k)*size + j+l]] * exp(-ssim[hf_array[i*size+j]*512+hf_array[(i+k)*size+j+l]]);
	      }
	}
      }
    }
    ene_b = w_s * ene_b + ssim[b*512+hf_array[i*size+j]];
    if(b/16%2 != qr_array[i*size+j]%2){
      ene_b += 100;
    }
    
    return ene_b - ene_a;
  }else{
    return 0.0;
  }
}

void write_data(int* data,char* name,int size){
  FILE *f;
  int i,j;
  f = fopen(name,"w");
  if(f == NULL){
    puts("open5");
    return;
  }
  for(i=0;i<size;i++){
    for(j=0;j<size;j++){
      fprintf(f,"%d\n",data[i*size+j]);
    }
  }
  if(fclose(f) == EOF){
    puts("close");
    exit -1;
  }
  return;
}

int main(){
  double* ssim;
  int* hf_array;
  int* data_array;
  int* qr_array;
  char buf[100];
  FILE *f1;
  FILE *f2;
  FILE *f3;
  FILE *f4;
  int i, j,k,l;
  int succ = 1;
  int size;
  double score;
  double best;
  int position;
  int count = 0;

  f1 = fopen("./data/matdata2.txt","r");
  if(f1 == NULL){
    puts("open1");
    return -1;
  }

  f2 = fopen("./temp_out/hf_mat.txt","r");
  if(f2 == NULL){
    puts("open2");
    return -1;
  }

  f3 = fopen("./temp_out/hf_mat.txt","r");
  if(f3 == NULL){
    puts("open3");
    return -1;
  }

  f4 = fopen("./temp_out/qr_mat.txt","r");
  if(f4 == NULL){
    puts("open4");
    return -1;
  }

  if(fgets(buf, 100, f4) != NULL){
    size = atoi(buf)*4+17;
  }

  printf("size : %d\n",size);

  ssim = (double*)malloc(sizeof(double)*512*512);
  if(ssim == NULL){
    puts("malloc1");
    return -1;
  }
  
  hf_array = (int*)malloc(sizeof(int)*size*size);
  if(hf_array == NULL){
    puts("malloc2");
    return -1;
  }
  
  data_array = (int*)malloc(sizeof(int)*size*size);
  if(data_array == NULL){
    puts("malloc3");
    return -1;
  }

  qr_array = (int*)malloc(sizeof(int)*size*size);
  if(qr_array == NULL){
    puts("malloc4");
    return -1;
  }

  for(i = 0 ; i < 512 ; i++){
      for(j = 0 ; j < 512 ; j++){
	if(fgets(buf, 100, f1) != NULL){
	  ssim[i*512+j] = atof(buf);
	}
      }
  }

  for(i = 0 ; i < size ; i++){
      for(j = 0 ; j < size ; j++){
	if(fgets(buf, 100, f2) != NULL){
	  hf_array[i*size+j] = atoi(buf);
	}
      }
  }

  for(i = 0 ; i < size ; i++){
      for(j = 0 ; j < size ; j++){
	if(fgets(buf, 100, f3) != NULL){
	  data_array[i*size+j] = atoi(buf);
	  printf("%3d",data_array[i*size+j]);
	}
      }
      puts("");
  }

  for(i = 0 ; i < size ; i++){
      for(j = 0 ; j < size ; j++){
	if(fgets(buf, 100, f4) != NULL){
	  qr_array[i*size+j] = atoi(buf);
	}
      }
  }

  // loading finished
  sprintf(buf,"init.txt");
  write_data(data_array,buf,size);

  while(succ == 1){
    succ = 0;
    for(i=0;i<512;i++){
      for(j=0;j<512;j++){
 	best = 0.0;
	position = -1;
	for(k=0;k<size;k++){
	  for(l=0;l<size;l++){
	    score = energy_label_set(k,l,i,j,hf_array,data_array,qr_array,ssim,size);
	    if(score < best){
	      succ = 1;
	      position = k*size+l;
	      best = score;
	    }
	  }
	}
	if(position != -1){
	  data_array[position] = j;
	}
      }
    }
    if(count%10 == 0){
      sprintf(buf,"./temp_out/%d.txt",count);
      write_data(data_array,buf,size);
    }
    printf("%d th loop\n",count);
    count++;
  }
  sprintf(buf,"./temp_out/output.txt");
  write_data(data_array,buf,size);
  return 0;
}
