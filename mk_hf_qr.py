#!/usr/bin/python
# -*- coding:utf-8 -*-

import sys
import subprocess

# argv[1] -> imput image
# argv[2] -> mask image
# argv[3] -> version
# argv[4] -> data string
# argv[5] -> output file name

if __name__ == '__main__':
    subprocess.call(['python','data/input.py',sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4]])
    subprocess.call(['data/calc'])
    subprocess.call(['python','data/output.py',sys.argv[5]])
