このレポジトリは、講義(演習3)の一環として、私が実装したHalftone QR Codesのソースコードを置いているレポジトリです。

・元の論文  
> Chu, Hung-Kuo, et al. "Halftone QR codes." ACM Trans. Graph. 32.6 (2013): 217.  
> ( http://cgv.cs.nthu.edu.tw/Projects/Recreational_Graphics/Halftone_QRCodes/ )

・注意点  
オリジナルの論文と異なり、エネルギー最適化の部分は単純なアルゴリズムを使っているため遅いです。  
また、λは0で決め打ちのため、論文中で主張されている、ユーザーによる調整が不能になっています。

・コンパイルに関して  
pythonでqrcodeというモジュールを利用しています。  
calc.c内ではmath.hを利用しています。  
> pip install qrcode  
> gcc calc.c -lm -o calc  

・その他  
プログラミングを始めて日が浅い時に書いたコードなので、ツッコミどころがたくさんあります。(pythonでoptparseを使っていないなど)  
ライセンスに関しては、元の論文に従います。  
